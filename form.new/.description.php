<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("FORM_NEW_NAME"),
	"DESCRIPTION" => GetMessage("FORM_NEW_DESC"),
	"ICON" => "/images/news_list.gif",
	"SORT" => 20,

	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "Makeit",
		"CHILD" => array(
			"ID" => "social_links",
			"NAME" => "Разное",
			"SORT" => 10,
		),
	),
);

?>