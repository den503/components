<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

$fieldsList = array(
	"name" => "Имя",
	"last_name" => "Фамилия",
	"second_name" => "Отчество",
	"full_name" => "ФИО",
	"email" => "Email",
	"url" => "Url",
	"message" => "Сообщение",
	"file" => "Файл",
);

$fieldsTypes = array(
	"text" => "Текст",
	"tel" => "Телефон",
	"email" => "Email",
	"textarea" => "Текстовое поле",
	"file" => "Файл",
	"radio" => "Переключатель",
	"checkbox" => "Флажок",
	"select" => "Выпадающий список",
	"hidden" => "Скрытое значение",
);

$captchaList = array(
	'bitrix' => 'Станадртная каптча Битрикс',
	'recaptcha' => 'reCAPTCHA',
);

$formTypes = array(
    "WEB_FORM" => "Веб форма",
    "EMAIL" => "Уведомление на почту",
    "B24" => "Лид в битрикс24",
);

$arComponentParameters = array(
	"GROUPS" => array(
		'captcha' => array(
			"NAME" => 'Безопастность'
		),
		'form' => array(
			"NAME" => 'Веб формы'
		)
	),
	"PARAMETERS" => array(
	
		"FIELDS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FIELDS"),
			"TYPE" => "TEXTAREA",
			/*"VALUES" => $fieldsList,*/
			"MULTIPLE" => "Y",
			"REFRESH" => "Y",
			"ADDITIONAL_VALUES" => "Y",
		),
		//использование капчи
		"USE_CAPTCHA" => array(
			"PARENT" => "captcha",
			"NAME" => GetMessage("USE_CAPTCHA"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y",
		),
		
		//использовать веб формы
		/*"USE_WEBFORM" => array(
			"PARENT" => "form",
			"NAME" => GetMessage("USE_WEBFORM_FIELD"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y",
		),*/
        "RESULT_TYPE" => array(
            "PARENT" => "form",
            "NAME" => GetMessage("RESULT_TYPE_FIELD"),
            "TYPE" => "LIST",
            "VALUES" => $formTypes,
            "REFRESH" => "Y",
        ),
		
		//выбор почтового шаблона для письма получателю
		//нужно ли дублировать письмо отправителю, если да, то какой шаблон

		"AJAX_MODE" => array(),

	),
);

//form type
if($arCurrentValues["RESULT_TYPE"] == "WEB_FORM"){
    $arComponentParameters['PARAMETERS']['FORM_NAME'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("FORM_NAME"),
        "TYPE" => "TEXT",
    );
	$arComponentParameters['PARAMETERS']['WEBFORM_ID'] = array(
		"PARENT" => "form",
		"NAME" => GetMessage("WEBFORM_ID_TYPE"),
		"TYPE" => "TEXT",
		"VALUES" => $captchaList,
		
	);
}elseif($arCurrentValues["RESULT_TYPE"] == "EMAIL"){
    //event id
    $arComponentParameters['PARAMETERS']['FORM_NAME'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("FORM_NAME"),
        "TYPE" => "TEXT",
    );
    $arComponentParameters['PARAMETERS']['EMAIL_EVENT_ID'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("EMAIL_EVENT_ID"),
        "TYPE" => "TEXT",
    );

    $arComponentParameters['PARAMETERS']['EMAIL_TEMPLATE_ID'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("EMAIL_TEMPLATE_ID"),
        "TYPE" => "TEXT",
    );
}elseif($arCurrentValues["RESULT_TYPE"] == "B24"){
    //lead bitrix 24
    /*
     * Name of bitrix wmkt.bitrix24.ru
     *  login, pass, title of lead (with varchars #VAR#)
     *
     */
    $arComponentParameters['PARAMETERS']['FORM_NAME'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("FORM_NAME"),
        "TYPE" => "TEXT",
    );
    $arComponentParameters['PARAMETERS']['B24_RESULT_NAME'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("B24_RESULT_NAME"),
        "TYPE" => "TEXT",
    );
    $arComponentParameters['PARAMETERS']['B24_RESULT_LOGIN'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("B24_RESULT_LOGIN"),
        "TYPE" => "TEXT",
    );
    $arComponentParameters['PARAMETERS']['B24_RESULT_PASS'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("B24_RESULT_PASS"),
        "TYPE" => "TEXT",
    );
    $arComponentParameters['PARAMETERS']['B24_RESULT_TITLE'] = array(
        "PARENT" => "form",
        "NAME" => GetMessage("B24_RESULT_TITLE"),
        "TYPE" => "TEXT",
    );
}


//варианты каптчи
if($arCurrentValues["USE_CAPTCHA"] == "Y"){
	
	$arComponentParameters['PARAMETERS']['CAPTCHA_TYPE'] = array(
		"PARENT" => "captcha",
		"NAME" => GetMessage("CAPTCHA_TYPE"),
		"TYPE" => "LIST",
		"VALUES" => $captchaList,
		"REFRESH" => "Y",
	);
	
	if($arCurrentValues["CAPTCHA_TYPE"] == "recaptcha"){
		$arComponentParameters['PARAMETERS']['recaptcha_KEY1'] = array(
			"PARENT" => 'captcha',
			"NAME" => GetMessage("recaptcha_KEY1"),
			"TYPE" => "TEXT",
		);
		$arComponentParameters['PARAMETERS']['recaptcha_KEY2'] = array(
			"PARENT" => 'captcha',
			"NAME" => GetMessage("recaptcha_KEY2"),
			"TYPE" => "TEXT",
		);
		
	}
	
}




if(count($arCurrentValues["FIELDS"])){
	//добавляем группы
	foreach($arCurrentValues["FIELDS"] as $k=>$v){
		if(!strlen($v))return;
		
		$arComponentParameters['GROUPS'][$v] = array(
			"NAME" => "Поле - ".$v,
			"SORT" => 5
		);
		//создаем нужные поля для каждого элемента
		//тип - textarea,text,file, radio , checkbox
		//проверка - регялярное выражение или уже готовая функция
		//обязательное ли поле
		//минимальная длина, максимальная длина
		//значение поля в почтовом шаблоне
		
		$arComponentParameters['PARAMETERS']['TYPE-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("TYPE_FIELD"),
			"TYPE" => "LIST",
			"VALUES" => $fieldsTypes,
			"DEFAULT" => "text",
			"REFRESH" => "Y",
		);
		
		//узнаем тип поля
		$fieldType = $arCurrentValues['TYPE-'.$v];
		
		
		//обязательное поле
		$arComponentParameters['PARAMETERS']['REQ-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("REQ_FIELD"),
			"TYPE" => "CHECKBOX",
		);
		
		//название поля name=""
		$arComponentParameters['PARAMETERS']['NAME-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("NAME_FIELD"),
			"TYPE" => "TEXT",
		);
        //Залоговоок поля
        $arComponentParameters['PARAMETERS']['LABEL-'.$v] = array(
            "PARENT" => $v,
            "NAME" => GetMessage("LABEL_FIELD"),
            "TYPE" => "TEXT",
        );
		if($fieldType == 'hidden'){
		    //значение поля
            $arComponentParameters['PARAMETERS']['VALUE-'.$v] = array(
                "PARENT" => $v,
                "NAME" => GetMessage("VALUE_FIELD"),
                "TYPE" => "TEXT",
                "DEFAULT" => "",
                "ADDITIONAL_VALUES" => "Y",
            );
			
			//Описание поля
			$arComponentParameters['PARAMETERS']['DESC-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("DESC_FIELD"),
				"TYPE" => "TEXT",
			);
		}
		//Сортировка
		$arComponentParameters['PARAMETERS']['SORT-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("SORT_FIELD"),
			"TYPE" => "TEXT",
		);
		//Placeholder
		$arComponentParameters['PARAMETERS']['PLACEHOLDER-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("PLACEHOLDER_FIELD"),
			"TYPE" => "TEXT",
		);
		
		//для этих полей возможно создание регулярных выражений
		if($fieldType == 'textarea' || $fieldType == 'text'){
			$arComponentParameters['PARAMETERS']['REGEX-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("REGEX_FIELD"),
				"TYPE" => "TEXT",
			);
		}
		
		if($fieldType == 'text' || $fieldType == 'tel' || $fieldType == 'email'){
			//маска ввода информации, например для телефона, даты и прочего
			$arComponentParameters['PARAMETERS']['MASK-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("MASK_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "",
			);
		}
		
		//макс длина поля
		if($fieldType == 'textarea' || $fieldType == 'text'){
			$arComponentParameters['PARAMETERS']['MAX-LEN-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("MAX_LEN_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "",
			);
		}
		
		//radio, select, checkbox значения
		if($fieldType == 'radio' || $fieldType == 'select' || $fieldType == 'checkbox'){
			$arComponentParameters['PARAMETERS']['KEY-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("KEY_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "",
				"ADDITIONAL_VALUES" => "Y",
			);
			//тперь нам нужно узнать сколько ключей уже вписано и создать поля для этих значений
			$arComponentParameters['PARAMETERS']['VALUE-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("VALUE_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "",
				"ADDITIONAL_VALUES" => "Y",
			);
		}
		
		
		//file
		if($fieldType == 'file'){
			//множественный выбор
			$arComponentParameters['PARAMETERS']['MULTIPLE-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("MULTIPLE_FILE_FIELD"),
				"TYPE" => "CHECKBOX",
			);
			//max count
			$arComponentParameters['PARAMETERS']['MAX_FILES-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("MAX_FILES_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "10",
			);
			//files support
			$arComponentParameters['PARAMETERS']['FILES_SUPPORT-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("FILES_SUPPORT_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "png, jpg, jpeg, bmp, gif",
			);
			//max file size
			$arComponentParameters['PARAMETERS']['MAX_FILE_SIZE-'.$v] = array(
				"PARENT" => $v,
				"NAME" => GetMessage("MAX_FILE_SIZE_FIELD"),
				"TYPE" => "TEXT",
				"DEFAULT" => "10000",
			);
		}
		
		
		//переменная для почтового шаблона
		/*$arComponentParameters['PARAMETERS']['TEMPLATE_SENDING-'.$v] = array(
			"PARENT" => $v,
			"NAME" => GetMessage("TEMPLATE_SENDING_FIELD"),
			"TYPE" => "TEXT",
		);*/

	}
	
		
}
