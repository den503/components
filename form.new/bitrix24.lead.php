<?

//prepare to CRM
$formValues = array();
$formTitleVarchars = array();
foreach ($arResult['FIELDS'] as $k => $v) {
    $formValues[strtoupper($v['NAME'])] = $v['VALUE'];
    $formTitleVarchars["#".strtoupper($v['NAME'])."#"] = $v['VALUE'];
}
//title varchars
$search  = array_keys($formTitleVarchars);
$replace = array_values($formTitleVarchars);

$arParams["B24_RESULT_TITLE"] = str_replace($search, $replace, $arParams["B24_RESULT_TITLE"]);



$B24Host = $arParams["B24_RESULT_NAME"].".bitrix24.ru";
$B24Path = "/crm/configs/import/lead.php";
$B24Port = 443;

/*
 *
 * В параметрах должны быть соответсвующие поля
 *
 */
$leadVarchars = array(
    "LOGIN" => $arParams["B24_RESULT_LOGIN"],//require
    "PASSWORD" => $arParams["B24_RESULT_PASS"],//require
    "TITLE" => $arParams["B24_RESULT_TITLE"],//require
    "COMPANY_TITLE" => "",
    "NAME" => "",
    "LAST_NAME" => "",
    "SECOND_NAME" => "",
    "POST" => "",
    "ADDRESS" => "",
    "COMMENTS" => "",
    "SOURCE_DESCRIPTION" => "",
    "STATUS_DESCRIPTION" => "",
    "OPPORTINUTY" => "",
    "CURRENCY_ID" => "",
    "PRODUCT_ID" => "",
    "SOURCE_ID" => "",
    "STATUS_ID" => "",
    "ASSIGNED_BY_ID" => "",
    "PHONE_WORK" => "",
    "PHONE_MOBILE" => "",
    "PHONE_FAX" => "",
    "PHONE_HOME" => "",
    "PHONE_PAGER" => "",
    "PHONE_OTHER" => "",
    "WEB_WORK" => "",
    "WEB_HOME" => "",
    "WEB_FACEBOOK" => "",
    "WEB_LIVEJOURNAL" => "",
    "WEB_TWITTER" => "",
    "WEB_OTHER" => "",
    "EMAIL_WORK" => "",
    "EMAIL_HOME" => "",
    "EMAIL_OTHER" => "",
    "IM_SKYPE" => "",
    "IM_ICQ" => "",
    "IM_MSN" => "",
    "IM_JABBER" => "",
    "IM_OTHER" => "",
);
$leadVarchars = array_merge($leadVarchars,$formValues);
$leadVarchars = array_diff($leadVarchars, array(''));//delete empty




// open socket to CRM
$fp = fsockopen("ssl://".$B24Host, $B24Port, $errno, $errstr, 30);
if ($fp) {
    // prepare POST data
    $strPostData = '';
    foreach ($leadVarchars as $key => $value)
        $strPostData .= ($strPostData == '' ? '' : '&').$key.'='.urlencode($value);

    // prepare POST headers
    $str = "POST ".$B24Path." HTTP/1.0\r\n";
    $str .= "Host: ".$B24Host."\r\n";
    $str .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $str .= "Content-Length: ".strlen($strPostData)."\r\n";
    $str .= "Connection: close\r\n\r\n";

    $str .= $strPostData;



    // send POST to CRM
    fwrite($fp, $str);

    // get CRM headers
    $result = '';
    while (!feof($fp)) {
        $result .= fgets($fp, 128);
    }
    fclose($fp);

    // cut response headers
    $arResult["B24_RESULT"] = explode("\r\n\r\n", $result);

    $arResult["B24_RESULT"] = json_decode(str_replace("'","\"",$arResult["B24_RESULT"][1]),true);


    if($arResult["B24_RESULT"]["ID"]){
        $arResult['SUCCESS'] = "Y";
    }else{
        $arResult['SUCCESS'] = "N";
    }

}
else {
    $arResult['SUCCESS'] = "N";
    //echo 'Connection Failed! '.$errstr.' ('.$errno.')';
}


?>