<?
//нужно удалить поле минимально кол-во файлов

/*
	Что нужно сделать:
		- обработку checkbox, radio, select. select multiple
		- работа с формой не как с Веб формой, а как с с почтовым шаблоном
		- работа с формой, добавление элемента, это когда нужно будет только для заказчика
		- файл, работа с файлом без участия dropzonejs (другой шаблон)
		- переместить подключаемые файлы из component_epilog.php
		- сделать обработку обычной каптчи
		- оптимизировать параметры компонента
		- добавить возможность показа каптчи только неавторизированным
		- сделать подключение скриптов опциональным
		- стандартный ajax битрикса удаляет script ы из получаемого html, что очень плохо. Нужна альтернатива
		
	Ошибки:
		- recaptcha js ошибка блокировки сайта





*/


if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Page\Asset;
$arResult = array();

//работа с веб формами
if($arParams['USE_WEBFORM'] == 'Y'){
	if(isset($arParams['WEBFORM_ID'])){
		if(!CModule::IncludeModule("form")){
			//не установлен модуль
		}
	}else{
		//не указан id формы, покажем это
	}
}

//подключаем капчу если нужно
if ($arParams['USE_CAPTCHA'] == "Y"){
	//нужно дать возможность использовать обычную капчу или recaptcha
	if($arParams['CAPTCHA_TYPE'] == 'bitrix'){
		$arResult['CAPTCHA_CODE'] = htmlspecialchars($GLOBALS["APPLICATION"]->CaptchaGetCode()); 
		$arResult['CAPTCHA_IMG_SRC'] = '/bitrix/tools/captcha.php?captcha_sid='.htmlspecialchars($GLOBALS["APPLICATION"]->CaptchaGetCode()); 
		$arResult['CAPTCHA_NAME'] = 'captchaField';
		$arResult['CAPTCHA_CODE_NAME'] = 'captchaSID';
	}elseif($arParams['CAPTCHA_TYPE'] == 'recaptcha'){
		$arResult['CAPTCHA_KEY1'] = $arParams['recaptcha_KEY1'];
		$arResult['CAPTCHA_KEY2'] = $arParams['recaptcha_KEY2'];
	}
}
$arResult['USE_CAPTCHA'] = $arParams['USE_CAPTCHA'];
$arResult['CAPTCHA_TYPE'] = $arParams['CAPTCHA_TYPE'];

//список регулярных выражений
$arRegEx = array(
	"email" => '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
	"numbers" => '/^[0-9]+$/',
	"ru" => '/[ А-Яа-я ]/',
	"en" => '^[\p{Latin}[A-Za-z]+$',
	"letters" => '',
	"url" => '',
	"phone" => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/',
);





//для начала нам нужно собрать все параметры и превести к правильному виду
$fieldsNames = $arParams['FIELDS'];
//удаляем пустые значения, если есть
$fieldsNames = array_diff($fieldsNames, array(''));



//var_dump($arParams);
//перебираем все параметры для получения значений
$arResult['FIELDS'] = array();
foreach($arParams as $k=>$v){
	//последнее вхождение
	$lastEl = strripos($k,'-');

	//if($lastEl != false && $lastEl != '_'){ странное условие
    if($lastEl != false && $lastEl != '-'){
		$fieldName = substr($k,$lastEl+1);
		//если есть в массиве, значит это параметр поля
		if(in_array($fieldName,$fieldsNames)){
			
			$propertyName = substr($k,0,$lastEl);
			if(!is_array($arResult['FIELDS'][$fieldName])){
				$arResult['FIELDS'][$fieldName] = array();
			}
			$arResult['FIELDS'][$fieldName][$propertyName] = $v;
		}
	}
}

$minTime = 3;
$timeOnSiteLast = $minTime;
if(CModule::IncludeModule("statistic")){
    //если есть модуль статистики то проверяем сколько секунд проведено на сайте
    $guest = CGuest::GetByID($_SESSION["SESS_GUEST_ID"]);
    $guest = $guest->Fetch();
    if($guest["ID"]){
        $timeOnSiteLast = intval($guest["LSESSION_TIME"]);
    }else{
        $timeOnSiteLast = 0;
    }
    //Передаем информацию о пользователе, чтобы отрыть форму в нужный момент
    $arResult["GUEST"] = $guest;
}


//присываем значения, если они передаются
if($_SERVER['REQUEST_METHOD'] == "POST" && $_POST['FORMA_SEND'] == "Y" && $_POST['FORM_NAME'] == $arParams['FORM_NAME']){

	$arResult['HAVE_ERRORS'] = false;
    //получаем данные о пользователе если есть модуль статистики


    //initLog("bot_form",array("time-on-site"=>$timeOnSiteLast,'guest'=>$guest));
    //var_dump($timeOnSiteLast);
    //bot check
    /*if($timeOnSiteLast <= $minTime && CModule::IncludeModule("statistic")) {
        $arResult['HAVE_ERRORS'] = true;
        $arResult['ERRORS'] = "Кажется вы спамбот. Если это не так, пожалуйста сообщите нам о данной проблеме, извините за сложившуюся ситуацию.";
    }*/
    include (__DIR__.'/form.check.php');






	//Значит все поля указаны правильно и делаем то что должна делать форма
	if(!$arResult['HAVE_ERRORS']){
		
		//работа с почтой
		//вызываем событияе отправки формы
		//это должно происходить перед отпавкой письма, чтобы значения формы можно поменять перед отправкой
		/*foreach (GetModuleEvents("makeitdatools", "OnAfterMakeitdaToolsFormAccept", true) as $arEvent){
			if (ExecuteModuleEventEx($arEvent, array(&$arResult['FIELDS']))===false)
				return false;
		}*/



        //вызываем осбытие модерации
        foreach (GetModuleEvents("makeitdatools", "OnAfterMakeitdaToolsFormAccept", true) as $arEvent)
            ExecuteModuleEventEx($arEvent, array(&$arResult['FIELDS']));

		//работа с формой
		//в случае с файлом файлы добавляются с цыфры, если файлов много, соответвенно нужно создавать поля file_1, file_2 ....
		//все нужные поля формы

        if($arParams['RESULT_TYPE'] == 'WEB_FORM') {
            $webForm = array();
            $rsQuestions = CFormField::GetList(1, "N", $by = "s_sort", $order = "asc", array('ACTIVE' => 'Y'));
            while ($arQuestion = $rsQuestions->Fetch()) {
                $webForm[$arQuestion['VARNAME']] = $arQuestion;
            }


            //перерь нужно переработать все поля, для того чтобы они подошли под формат "Веб формы"
            $formValues = array();
            foreach ($arResult['FIELDS'] as $k => $v) {
                if ($v['TYPE'] == 'file') {
                    foreach ($v['VALUE'] as $key => $val) {
                        $formValues[$v['NAME'] . '_' . ($key + 1)] = array(
                            "TYPE" => $v['TYPE'],
                            "VALUE" => CFile::MakeFileArray($val),
                            "NAME" => $webForm[$v['NAME'] . '_' . ($key + 1)]['ID']
                        );
                    }
                } elseif ($v['TYPE'] == 'checkbox' || $v['TYPE'] == 'radio') {
                    $formValues[$v['NAME']] = array(
                        "TYPE" => $v['TYPE'],
                        "VALUE" => $v['VALUE'],
                        "NAME" => $webForm[$v['NAME']]['SID']
                    );
                } elseif ($v['TYPE'] == 'select') {
                    if ($v['MULTYPLE'] == "Y") {
                        //array value
                        $formValues[$v['NAME']] = array(
                            "TYPE" => 'multiselect',
                            "VALUE" => $v['VALUE'],
                            "NAME" => $webForm[$v['NAME']]['SID']
                        );
                    } else {
                        $formValues[$v['NAME']] = array(
                            "TYPE" => 'dropdown',
                            "VALUE" => $v['VALUE'],
                            "NAME" => $webForm[$v['NAME']]['SID']
                        );
                    }
                } else {
                    $formValues[$v['NAME']] = array(
                        "TYPE" => $v['TYPE'],
                        "VALUE" => $v['VALUE'],
                        "NAME" => $webForm[$v['NAME']]['ID']
                    );
                }
            }

            //совмещаем массивы
            $resultFields = array();
            foreach ($formValues as $k => $v) {
                $resultFields['form_' . $v['TYPE'] . '_' . $v['NAME']] = $v['VALUE'];
            }

            if($RESULT_ID = CFormResult::Add($arParams['WEBFORM_ID'], $resultFields	)){
                //выводим успешное заполенение формы
                $arResult['SUCCESS'] = "Y";
            }else{
                //выводим неудачную форму
                $arResult['SUCCESS'] = "N";
            }
        }elseif($arParams['RESULT_TYPE'] == 'B24'){
            //b24 crm
            include(__DIR__."/bitrix24.lead.php");
        }elseif($arParams['RESULT_TYPE'] == 'EMAIL'){
            //email
            $event = $arParams["EMAIL_EVENT_ID"];
            $lid = SITE_ID;
            $arFields = array();
            $src = array();
            foreach ($arResult['FIELDS'] as $k => $v) {
                if ($v['TYPE'] == 'file') {
                    foreach ($v["VALUE"] as $id) {
                        $fileArray = CFile::GetFileArray($id);
                        $src[] = '<a href="http://'.$_SERVER["SERVER_NAME"].$fileArray["SRC"].'">'.$fileArray["ORIGINAL_NAME"].'</a>';
                    }
                    $arFields[$k] = $src;
                } else {
                    $arFields[$k] = $v["VALUE"];
                }

            }
            $message_id = $arParams["EMAIL_TEMPLATE_ID"];


            if($RESULT_ID = CEvent::SendImmediate($event, $lid, $arFields, $Duplicate = "N", $message_id)){
                //выводим успешное заполенение формы
                $arResult['SUCCESS'] = "Y";
            }else{
                //выводим неудачную форму
                $arResult['SUCCESS'] = "N";
            }

        }elseif($arParams['RESULT_TYPE'] == 'ELEMENT'){
            /*
             * Написать обработчик для создания элемента в системе
             *
             */
        }




		//var_dump($resultFields);

		
		// создадим новый результат
        /*
         * Созраняем результат, возможные значения:
         * Веб форма, почтовое событие и bitrix24 CRM
         *
         *
         * bitrix24.com/crm/configs/import/lead.php
         */

		
		//var_dump($RESULT_ID);
		//var_dump($arResult['SUCCESS']);
		
	}
}









//это поле должно быть здесь, потому что оно будет еще проверяться на значение
/*$arResult['FIELDS']['BX_SID'] = array(
	"TYPE" => "hidden",
	"REQ" => "Y",
	"NAME" => "BX_SID",
);*/








//добавляем нужные дополнительные поля
$arResult['FIELDS']['BX_SID'] = array(
	"TYPE" => "hidden",
	"REQ" => "Y",
	"VALUE" => bitrix_sessid(),
	"NAME" => "BX_SID",
	"SORT" => 0
);
$arResult['FIELDS']['FORMA_SEND'] = array(
	"TYPE" => "hidden",
	"VALUE" => "Y",
	"NAME" => "FORMA_SEND",
	"SORT" => 0
);
$arResult['FIELDS']['FORM_NAME'] = array(
    "TYPE" => "hidden",
    "VALUE" => $arParams['FORM_NAME'],
    "NAME" => "FORM_NAME",
    "SORT" => 0
);


$sortAr = array();
//нужно отсортировать форму, 3 поля выше должны быть первыми
foreach($arResult['FIELDS'] as $k=>$v){
	$sort = intval($v['SORT'])*10000;
	if(isset($sortAr[$v['SORT']])){$sort += $k+1;}
	$v['ID'] = $k; 
	$sortAr[$sort] = $v;
}
ksort($sortAr);

//для того, чтобы можно было бьло обращаться непосредственно к компоненту, нужно указать путь к нему
$componentPath = strpos(__DIR__,'/bitrix/components');
$componentPath = substr(__DIR__,$componentPath);

$arResult['FILE_UPLOAD_PATH'] = $componentPath.'/files.upload.php';

$arResult['FIELDS'] = array();
foreach($sortAr as $k=>$v){
	$id = $v['ID'];
	unset($v['ID']);
	if($v['TYPE'] == 'file'){
		$v['FILE_UPLOAD_PATH'] = $arResult['FILE_UPLOAD_PATH'];
	}
	$arResult['FIELDS'][$id] = $v;
}



//Я остановился на подключении НУЖНЫХ СКРИПТОВ в нужны ймомент


Asset::getInstance()->addCss($componentPath."/css/dropzone.css");

Asset::getInstance()->addJs($componentPath."/js/dropzone.js");
Asset::getInstance()->addJs($componentPath."/js/jquery.mask.min.js");


/*$APPLICATION->SetAdditionalCSS($componentPath."/css/dropzone.css");
$APPLICATION->AddHeadScript($componentPath."/js/dropzone.js");
$APPLICATION->AddHeadScript($componentPath."/js/jquery.mask.min.js");*/
if($arParams['CAPTCHA_TYPE'] == 'recaptcha' && $arParams['USE_CAPTCHA'] == "Y"){
	//$APPLICATION->AddHeadString('<script src="//www.google.com/recaptcha/api.js?onload=captchaJsLoad&render=explicit"></script>',true);
}
	
$this->IncludeComponentTemplate();