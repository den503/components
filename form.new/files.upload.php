<?
//очищаем все то, что было ранее
define("NO_KEEP_STATISTIC", true);
define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


//компонент подразумевает работу с загрузчкином файлов
//это обзначает что файлы будут передаваться еще до того, как форма будет поллносью загружена
//фактически загрузчик можно выбрать любой, главное, чтобы он пнрндавал правильно файлы
//перебираем все файлы из переменной и сохраняем их

$arFiles = array();


header('Content-type: text/json');              
header('Content-type: application/json');



if(count($_FILES)){
	//создание файлов
	foreach($_FILES as $k=>$v){
		//проверяем на размер файла и соотвтетствию типу файлов
		$v['MODULE_ID'] = 'makeitdatools';
		$v['del'] = 'N';
		//порверяем, соответсвует ли файл требованиям
		$arFiles[] = array("FILE_ID" => CFile::SaveFile($v, 'makeitdatools'));
	}
	echo json_encode($arFiles);
}elseif($_REQUEST['files']){
	//получение файлов
	$filesID = array();
	if(strpos($_REQUEST['files'],',') > 0){
		$filesID = explode(',',$_REQUEST['files']);
		foreach($filesID as &$v){$v = intval($v);}
	}else{
		$filesID[] = intval($_REQUEST['files']);
	}
	if(count($filesID)){
		$res = CFile::GetList(array("ID"=>"asc"), array("@ID" => $filesID));
		while($file = $res->GetNext()){
			$arFiles[] = $file;
		}
	}
	echo json_encode($arFiles);
}




//нужно сделать очиститель старых файлов, которые не используются
//проверять по дате создания
$res = CFile::GetList(array("FILE_SIZE"=>"desc"), array("MODULE_ID"=>"makeitdatools"));
$curDT = new DateTime("now");
while($file = $res->GetNext()){
	
	$fileDT = new DateTime($file['TIMESTAMP_X']);
	$interval = $curDT->diff($fileDT);
	
	//удаляем файлы больше 2 часов живущие тут
	if(intval($interval->format('%i')) > 120){
		CFile::Delete($file['ID']);
	}
}



//далее ничего не выводим
die();