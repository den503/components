<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//данный файл предназначен для проверки отправленных данных


//нужно собрать все передаваемые знаения и засунуть их в поля
$arEventFields = array();
foreach($arResult['FIELDS'] as $k =>& $v){
	if(array_key_exists($v['NAME'],$_POST)){
		
		
		//var_dump($arResult['FIELDS'][$k]);
		
		$v['ERRORS'] = array();
		
		$v['VALUE'] = $_POST[$v['NAME']];
		
		//нужно получить только ключи - значения для вставки в шаблон
		//$arEventFields[$v['TEMPLATE_SENDING']] = $_POST[$v['NAME']];
		
		//есть общие критерии, котоыре проверяются
		if($v['REQ'] == "Y" && empty($v['VALUE'])){
			//var_dump($v["NAME"]);
			$arResult['HAVE_ERRORS'] = true;
			$v['ERRORS'][] = 'Поле обязательно для заполения';
		}
		
		if($v['TYPE'] == 'text' || $v['TYPE'] == 'textarea'){


            if($v['REGEX'] == "phone"){$v['VALUE'] = preg_replace('/[^0-9]/', '', $v['VALUE']);}//fix for phone
			//если мы в регулярное выражение внисание название имещегося в компоненте, подставляем его
			if(isset($arRegEx[$v['REGEX']])){$v['REGEX'] = $arRegEx[$v['REGEX']];}

			
			
			//var_dump($v['REGEX']);
			
			if(!preg_match($v['REGEX'], $v['VALUE']) && strlen($v['REGEX'])){
				$arResult['HAVE_ERRORS'] = true;	
				//здесь нужна нормальная ошибка, которая будет браться из параметров
				$v['ERRORS']['WRONG_FORMAT'] = 'Не верно введены данные';
			}

		}
        if($v['TYPE'] == '' || $v['TYPE'] == 'textarea'){


            if($v['REGEX'] == "phone"){$v['VALUE'] = preg_replace('/[^0-9]/', '', $v['VALUE']);}//fix for phone
            //если мы в регулярное выражение внисание название имещегося в компоненте, подставляем его
            if(isset($arRegEx[$v['REGEX']])){$v['REGEX'] = $arRegEx[$v['REGEX']];}



            //var_dump($v['REGEX']);

            if(!preg_match($v['REGEX'], $v['VALUE']) && strlen($v['REGEX'])){
                $arResult['HAVE_ERRORS'] = true;
                //здесь нужна нормальная ошибка, которая будет браться из параметров
                $v['ERRORS']['WRONG_FORMAT'] = 'Не верно введены данные';
            }

        }


		
		
		
		
		//в зависимости от типа поля, по разному обрабатываем правильность заполнения
		switch ($v['TYPE']){
			case 'text':
				if($v['MIN_LEN'] > strlen($v['VALUE']) && $v['MIN_LEN']){
					$arResult['HAVE_ERRORS'] = true;
					$v['ERRORS'][] = 'Длина поля должна быть не менее '.$v['MIN_LEN'] ." символов.";
				}
				if($v['MAX_LEN'] < strlen($v['VALUE']) && $v['MAX_LEN']){
					$arResult['HAVE_ERRORS'] = true;
					$v['ERRORS'][] = 'Длина поля должна быть не более '.$v['MAX_LEN'] ." символов.";
				}
				
			break;

			/*case 'file':
				if($v['MULTIPLE'] == "Y"){
					$filesCount = count($v['VALUE']);
					if($v['MIN_FILES'] > $filesCount){
						$arResult['HAVE_ERRORS'] = true;
						$v['ERRORS'][] = 'Количество файлов должно быть не менее '.strlen($v['VALUE']);
					}
					if($v['MAX_FILES'] > $filesCount){
						$arResult['HAVE_ERRORS'] = true;
						$v['ERRORS'][] = 'Количество файлов должно быть не более '.strlen($v['VALUE']);
					}
					
					//проверяем на размер файла и соотвтетствию типу файлов
					foreach($v['VALUE'] as $k=>$v){
						$v['MODULE_ID'] = 'makeitdatools';
						$v['del'] = 'N';
						$res = CFile::CheckFile($v, $v['MAX_FILE_SIZE'], $v['type'], $v['FILES_SUPPORT']);
						if(strlen($res) > 0){
							$arResult['HAVE_ERRORS'] = true;
							$v['ERRORS'][] = $res;
						}
						
					}
					
				}
			
			break;*/
		}
		
	}
	unset($v);
}	

//проверяем капчу, если есть
if ($arParams['USE_CAPTCHA'] == "Y"){
	//нужно дать возможность использовать обычную капчу или recaptcha
	if($arParams['CAPTCHA_TYPE'] == 'bitrix'){
		$cptcha = new CCaptcha();
		
		if(!strlen($_POST[$arParams['CAPTCHA_NAME']])>0){ 
			$arResult['HAVE_ERRORS'] = true;
			$arResult['CAPTCHA_ERROR'] = 'Не введен защитный код';
		}elseif($cptcha -> CheckCode($_POST[$arParams['CAPTCHA_NAME']],$_POST[$arParams['CAPTCHA_CODE_NAME']])){
			$arResult['HAVE_ERRORS'] = true;
			$arResult['CAPTCHA_ERROR'] = 'Введен не верный защитный код';
		}
		
	}elseif($arParams['CAPTCHA_TYPE'] == 'recaptcha'){
		
		if(isset($_POST['g-recaptcha-response'])){
          $captcha = $_POST['g-recaptcha-response'];
        }
		
        if(!$captcha){
			$arResult['HAVE_ERRORS'] = true;
			$arResult['CAPTCHA_ERROR'] = 'Зайщита не пройдена';
        }
		
        $response = file_get_contents("http://www.google.com/recaptcha/api/siteverify?secret=".$arResult['CAPTCHA_KEY2']."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($response.success == false){
			$arResult['HAVE_ERRORS'] = true;
			$arResult['CAPTCHA_ERROR'] = 'Кажется, вы спаммер';
        }	
	}
}





//var_dump($arResult['FIELDS']);





//вызываем событияе отправки формы
//это должно происходить перед отпавкой письма, чтобы значения формы можно поменять перед отправкой
/*foreach (GetModuleEvents("makeitdatools", "OnAfterMakeitdaToolsFormAccept", true) as $arEvent){
	if (ExecuteModuleEventEx($arEvent, array(&$arResult['FIELDS']))===false)
		return false;
}*/


//CEvent::Send("ADV_CONTRACT_INFO", SITE_ID, $arEventFields);

	
	
