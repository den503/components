<?
$MESS["FIELDS"] = "Поля (В названии не может быть символ '-')";
$MESS["SORT_FIELD"] = "Сортировка";
$MESS["TYPE_FIELD"] = "Тип поля";
$MESS["REQ_FIELD"] = "Обязательное поле";
$MESS["NAME_FIELD"] = "Аттрибут name";
$MESS["LABEL_FIELD"] = "Название поля";
$MESS["VALUE_FIELD"] = "Значение поля";
$MESS["DESC_FIELD"] = "Описание поля";
$MESS["MIN_LEN_FIELD"] = "Минимальная длина скроки";
$MESS["MAX_LEN_FIELD"] = "Максимальная длина скроки";
$MESS["KEY_FIELD"] = "Значение";
$MESS["MULTIPLE_FILE_FIELD"] = "Множественное поле";
$MESS["MIN_FILES_FIELD"] = "Минимальное количество файлов";
$MESS["MAX_FILES_FIELD"] = "Максимальное количество файлов";
$MESS["FILES_SUPPORT_FIELD"] = "Поддерживаемые расширения файлов";
$MESS["MAX_FILE_SIZE_FIELD"] = "Максимальный размер файла (кб.)";
$MESS["TEMPLATE_SENDING_FIELD"] = "Переменная почтового шаблона";
$MESS["REGEX_FIELD"] = "Регулярное выражение для проверки";
$MESS["PLACEHOLDER_FIELD"] = "Плейсхолдер";
$MESS["USE_CAPTCHA"] = "Использовать captcha?";
$MESS["CAPTCHA_TYPE"] = "Тип captcha";
$MESS["recaptcha_KEY1"] = "Публичный ключ";
$MESS["recaptcha_KEY2"] = "Секретный ключ";
$MESS["MASK_FIELD"] = "Маска ввода (0 - переменная)";
$MESS["USE_WEBFORM_FIELD"] = "Исползовать веб-формы";
$MESS["WEBFORM_ID_TYPE"] = "ID веб формы";
$MESS["FORM_NAME"] = "Имя формы";



$MESS["RESULT_TYPE_FIELD"] = "Обработка результата";
$MESS["EMAIL_EVENT_ID"] = "ID почтового события";
$MESS["EMAIL_TEMPLATE_ID"] = "ID почтового шаблона";


$MESS["B24_RESULT_NAME"] = "Название сервера без '.bitrix24.com'";
$MESS["B24_RESULT_LOGIN"] = "Логин";
$MESS["B24_RESULT_PASS"] = "Пароль";
$MESS["B24_RESULT_TITLE"] = "Заголовок лида (Могут быть переменные, например, #COMPANY_TITLE#)";

?>
