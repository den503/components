



/*function createOrderSlider(){
	//пока обычный слайдер, потом нужно будет приколюхи делать
		var $frame = $('#order_wrapper');
		var scrollbar = $('#order_scroll');
		// Call Sly on frame
		window.orderSlider = new Sly($frame,{
			slidee:$frame.children('.slide'),
			speed: 300,
			easing: 'easeOutExpo',
			scrollBar: scrollbar,
			scrollBy: 100,
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
			scrollSource:$frame.children('.slide'),
		});
		
		//скрываем скроллбар
		window.orderSlider.on('load', function () {
		  // check if there is nowhere to scroll
			if (this.pos.start === this.pos.end) {
				scrollbar.hide();
			} else {
				scrollbar.show();
			}
		});	
		window.orderSlider.init()
}

function destroyOrderlider(){
	if(typeof window.orderSlider != 'undefined'){
		window.orderSlider.destroy();
	}
}
function reloadOrderlider(){
	if(typeof window.orderSlider != 'undefined'){
		window.orderSlider.reload();
	}
}*/


function componentFormAlways(files){
	
	$('#order input').each(function(){
		var self = $(this);
		if(typeof self.data('mask') == "string"){
			self.mask(self.data('mask'));
		}
	});
	
	
	//console.log(files);
	
	for(var i = 0; i < files.length; i++){
		var self = $(this);
		var file = files[i];
		//не получается получить параметры
		
		
		
		//даем понятные для плагина формат для файлов
		file.FILES_SUPPORT = file.FILES_SUPPORT.replace(/ /g,'');
		if(file.FILES_SUPPORT.indexOf(',')){
			file.FILES_SUPPORT = file.FILES_SUPPORT.split(',');
			if(typeof file.FILES_SUPPORT == 'object' || typeof file.FILES_SUPPORT == 'array'){
				for(var ii = 0; ii < file.FILES_SUPPORT.length; ii++){
					file.FILES_SUPPORT[ii] = '.'+file.FILES_SUPPORT[ii];
				}
				file.FILES_SUPPORT = file.FILES_SUPPORT.join(', '); 
			}
		}
		

		
		$("#"+file.NAME).dropzone({
                clickable:true,
				url: file.FILE_UPLOAD_PATH,
				paramName: file.NAME,
				maxFilesize:  file.MAX_FILE_SIZE,
				maxFiles: file.MAX_FILES,
				acceptedFiles: file.FILES_SUPPORT,

				init: function() {
					//console.log(file);
					//существующие файлы
					var thisDropzone = this;
					//размер слайдера
					/*thisDropzone.on("addedfile", function() {
						 reloadOrderlider();
					});
					thisDropzone.on("removedfile", function() {
						 reloadOrderlider();
					});*/



					if(typeof file.VALUE != 'undefined' && file.VALUE.length){
						var files = file.VALUE.join(',');
						$.get(file.FILE_UPLOAD_PATH+'?files='+files , function(data) {
							$.each(data, function(key,value){
								var mockFile = { name: value.ORIGINAL_NAME, size: value.FILE_SIZE, fileID:value.ID };
								thisDropzone.options.addedfile.call(thisDropzone, mockFile);
							});
							$(thisDropzone.element).find('.dz-preview').each(function(i,e){
								$(this).find('input').val(files[i]);
							});
						});
						//размер
						//reloadOrderlider();
					}




				},
				success:function(file, response){
					for(var i = 0; i < response.length; i++){
						$(file.previewElement).find('input').val(response[i].FILE_ID);
					}
					//размер

				},
				dictInvalidFileType: 'Недопустимый формат файла',
				dictCancelUploadConfirmation: 'Вы уверены, что хотите прервать загрузку файла?',
				dictMaxFilesExceeded: 'Можно добавить не больше '+file.MAX_FILES+' файлов',
				dictFileTooBig: 'Файл слишком большой - {{filesize}}Мб. Максимальный размер файла - {{maxFilesize}}Мб.',
				previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n <input type='hidden' name='"+file.NAME+"[]' value=''>\n  <div class=\"dz-details\">\n <div class=\"dz-filename\"><span class=\"dz-name\" data-dz-name></span><div class=\"dz-remove\" data-dz-remove></div></div>\n  </div>\n <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>",
		});
		
		
	};

	
	
	//ошибки
	var captchaEr = $('.make-error');
	captchaEr.removeClass('make-error');
	captchaEr.addClass('error');
	$('.g-recaptcha').mouseenter(function(){
		setTimeout(function(){
		captchaEr.removeClass('error');
		},500);
	})
	$('.need-error').each(function(i,e){
		var self = $(this);
		self.removeClass('need-error');	
		setTimeout(function(){
			self.addClass('error');	
		},i*150);
	})
	
	
	
	
	//обновляем слайдер
	//createOrderSlider();
}









BX.addCustomEvent('onComponentFormNewReady', function(files){
	componentFormAlways(files);
});


BX.ready(function(e) {
    Dropzone.autoDiscover = false;
    var b = $("body");
    var html = $('html');


	//opne-close
    b.on('click closeOrder','#make_order, #order .close, #order_place .overflow',function(){


		$('#order .error').removeClass('error');
		if(html.hasClass('mobile')){
			var self = $(this);
			if(self.hasClass('close')){
				html.removeClass('order_active');
			}else if(!html.hasClass('order_active')){
				html.addClass('order_active');
			}
			slideout.close();
		}else{
			html.toggleClass('order_active');
			if(html.hasClass("order_active")){
                $(document).trigger("form-open");
            }
		}

	});

	//close only
	$('#logo_wrapper, #menu').on('click',function(e){
		if(!html.hasClass('mobile')){
            html.removeClass('order_active');
			$('#order .error').removeClass('error');
		}
	})


	//show
    $("#order_placeholder").show();



});