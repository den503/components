<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

                

<!--noindex-->

<div id="order_place">
    <div id="order">
    	<div class="close">
            <svg height="29" width="28" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 29">
                <path fill="#9b9b9b" fill-rule="evenodd" d="M 0.92 0.71 L 0.21 1.42 L 13.29 14.5 L 0.21 27.58 L 0.92 28.29 L 14 15.21 L 27.08 28.29 L 27.79 27.58 L 14.71 14.5 L 27.79 1.42 L 27.08 0.71 L 14 13.79 L 0.92 0.71 Z M 0.92 0.71" />
            </svg>
        </div>
        <div class="one_more">
    	<div id="order_wrapper">

            <div class="order_padding">
			<? if($arResult['SUCCESS'] != "N" && $arResult['SUCCESS'] != "Y"):?>
                <h2 class="h1"><?=GetMessage("FORM_HEADER_NORMAL")?></h2>

                <div class="sub_header"><?=GetMessage("FORM_DESC_NORMAL")?></div>


                <div>
                <?

                    //если подключена веб форма
                    /*$resultFields = array();
                    $rsQuestions = CFormField::GetList(1,"N",$by="s_sort",$order="asc",array('ACTIVE'=>'Y'));
                    while ($arQuestion = $rsQuestions->Fetch()){
                        $resultFields[] = $arQuestion;
                    }*/

                ?>




                </div>
                <? if(strlen($arResult['ERRORS'])>0):?>
                    <div class="form-error">
                        <?=$arResult['ERRORS']?>
                    </div>
                <? endif?>



                <? if($_POST['FORMA_SEND'] == "Y"){/*var_dump($arResult['FIELDS']);*/}?>

                <form action="<?=$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/formdata">

                    <? foreach($arResult['FIELDS'] as $k=>$v):?>


                        <? if($v['TYPE'] == 'text'):?>
                            <div class="form_row <?=count($v['ERRORS'])?'need-error':''?> <?=$v['VALUE']?'filed':''?>">
                                <label for="<?=$k?>"><?=$v['LABEL']?></label>
                                <input type="text" id="<?=$k?>" name="<?=$v['NAME']?>" <?=$v['MASK']?'data-mask="'.$v['MASK'].'"':''?> value="<?=$v['VALUE']?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>">
                                <? if(count($v['ERRORS'])):?>
                                <ul class="errors">
                                    <? foreach($v['ERRORS'] as $error):?>
                                        <li><?=$error?></li>
                                        <? break?>
                                    <? endforeach?>
                                </ul>
                                <? endif?>
                            </div>
                        <? elseif($v['TYPE'] == 'textarea'):?>
                            <div class="form_row <?=count($v['ERRORS'])?'need-error':''?> <?=$v['VALUE']?'filed':''?>">
                                <label for="<?=$k?>"><?=$v['LABEL']?></label>
                                <textarea name="<?=$v['NAME']?>" id="<?=$k?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>"><?=$v['VALUE']?></textarea>
                                <? if(count($v['ERRORS'])):?>
                                <ul class="errors">
                                    <? foreach($v['ERRORS'] as $error):?>
                                        <li><?=$error?></li>
                                        <? break?>
                                    <? endforeach?>
                                </ul>
                                <? endif?>
                            </div>
                        <? elseif($v['TYPE'] == 'select'):?>

                        <? elseif($v['TYPE'] == 'radio'):?>

                        <? elseif($v['TYPE'] == 'checkbox'):?>

                        <? elseif($v['TYPE'] == 'file'):?>
                            <div class="files_wrap">

                                <div id="<?=$v['NAME']?>" class="dropzone dz-clickable">
                                    <div class="dz-default dz-message">
                                        <div>
                                            <svg id="SvgjsSvg1000" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="23" height="28"><defs id="SvgjsDefs1001"></defs><path id="SvgjsPath1007" d="M229 1056L229 1082L250 1082L250 1062.37L244.54 1056ZM228 1056L229 1056L229 1055ZM228 1082L229 1083L229 1082ZM250 1083L251 1082L250 1082ZM245 1056L244.24 1055.65L244.54000000000002 1056ZM250.24 1062.65L250 1062L250 1062.37ZM228 1055L245 1055L251 1062L251 1083L228 1083Z " fill="#222222" fill-opacity="1" transform="matrix(1,0,0,1,-228,-1055)"></path><path id="SvgjsPath1008" d="M244 1056L244 1063L250 1063L250 1062L245 1062L245 1056Z " fill="#222222" fill-opacity="1" transform="matrix(1,0,0,1,-228,-1055)"></path></svg>
                                        </div>
                                        <div>
                                            <p class="h"><b>Прикрепите файлы</b></p>
                                            <p class="t">Заполненый бриф, техническое задание или другие необходимые документы</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="brif">
                                    <a target="_black" href="https://makeit-da.ru/Бриф Makeit.docx">
                                        <div class="i">
                                            <!--svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27" height="32">
                                                <g fill="#000" fill-rule="evenodd">
                                                    <path d="M27 2H14.007v-.14c0-1.026-.834-1.86-1.86-1.86H9.984c-1.025 0-1.86.834-1.86 1.86V2h-3.01L5 7H.008v19.205l.01.1c.023.08.026.087.03.095.036.072.04.08.046.087.04.052.05.064.06.075l5.143 5.142.077.063.087.047.096.03c.066.008.082.008.1.008L21 32v-3h6V2zM9.983 1.014h2.165c.466 0 .845.38.845.846V2H9.138v-.14c0-.466.38-.846.845-.846zM9 8h2v2H9V8zm2.065 3c0 .576.078 1.324-1.065 1.324S9 11.678 9 11h2.065zM6 3h2.124v4H6V3zM5 30.1l-2.975-3.106H5V30.1zm15.026.98H6V26H1V8h7v2H4.032c-.28 0-.443.207-.443.503 0 .297.442.497.442.497H8c0 1.066.056 2.043 2.102 2.043s1.896-1.14 1.896-2.043h5.29s.457.007.457-.497c0-.503-.458-.503-.458-.503h-5.29V8h8.03V31.08zM26 28h-5V7h-9.002V4.294c0-.28-.2-.44-.48-.44-.282 0-.453.16-.453.44V7H9V3h17v25z"/>
                                                    <path d="M18 15H3v1.004h15M18 19H3v1.004h15M18 23H3v1.004h15"/>
                                                </g>
                                            </svg>
                                        </div>
                                        <p><b>Скачать бриф</b></p>
                                    </a>
                                </div>
                            </div>

                        <? elseif($v['TYPE'] == 'hidden'):?>
                            <input type="hidden" name="<?=$v['NAME']?>" id="<?=$k?>" value="<?=$v['VALUE']?>">
                        <? endif?>

                    <? endforeach?>



                    <div id="bottom_place">
                        <? if($arResult['USE_CAPTCHA'] == "Y"):?>
                        <div class="captcha_place <?=strlen($arResult['CAPTCHA_ERROR']) > 0 ?'make-error':''?>">
                            <? if($arResult['CAPTCHA_TYPE'] == 'bitrix'):?>
                                <img src="<?=$arResult['CAPTCHA_IMG_SRC']?>" alt="captcha">
                                <input type="hidden" name="<?=$arResult['CAPTCHA_CODE_NAME']?>" value="<?=$arResult['CAPTCHA_CODE']?>">
                            <? else:?>

                                <div class="g-recaptcha" id="recapID" data-sitekey="<?=$arResult['CAPTCHA_KEY1']?>"></div>

                                <? if(strlen($arResult['CAPTCHA_ERROR']) > 0):?>
                                    <div class="errors">
                                        <span><?=$arResult['CAPTCHA_ERROR']?></span>
                                    </div>
                                <? endif?>

                                <script>
                                var captchaJsLoad = function() {
                                    grecaptcha.render($('#recapID')[0], {
                                      'sitekey' : '<?=$arResult['CAPTCHA_KEY1']?>'
                                    });
                                    $('#order .captcha_place').addClass('show');
                                };
                                //перезагрузка каптчи
                                if(typeof grecaptcha == 'object'){
                                    captchaJsLoad();
                                }
                                </script>
                            <? endif ?>

                            <p>

                            </p>

                        </div>
                        <? endif?>
                        <div id="order_send">
                            <div>
                            <button value="Y" name="FORMA_SEND" type="submit" class="btn change-color solid-lines-in mobile-full">Отправить</button>
                            </div>
                        </div>
                    </div>
                </form>





            <? elseif($arResult['SUCCESS'] == "Y"):?>

                <div id="form_send">
                    <div class="icon">
                        <svg height="128" width="282" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 282 128">
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.13 0.34 C 84.13 0.34 281.94 0.34 281.94 0.34 C 281.94 0.34 281.94 127.71 281.94 127.71 C 281.94 127.71 84.24 127.71 84.24 127.71 C 84.24 127.71 84.13 0.34 84.13 0.34 Z" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.38 0.14 C 84.38 0.14 167.65 84.01 171.11 87.46 C 174.56 90.92 182.86 98.85 193.91 87.79 C 204.96 76.74 280.81 0.3 280.81 0.3 C 280.81 0.3 84.38 0.14 84.38 0.14 Z" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 280.75 127.24 C 280.75 127.24 217.91 64.4 217.91 64.4" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 84.74 126.56 C 84.74 126.56 147.4 63.9 147.4 63.9" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.96 33.85 C 73.96 33.85 0.53 33.85 0.53 33.85" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.92 64.77 C 73.92 64.77 45.25 64.77 45.25 64.77" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 74.09 94.77 C 74.09 94.77 23.09 94.77 23.09 94.77" />
                        </svg>
                    </div>



                    <div class="additional_info">
                        <div class="sub_header">
                            <h2 class="h1">Заявка успешно отправлена</h2>
                        </div>
                        <div class="sub_header">
                            В ближайшее время мы обработаем вашу заявку и свяжемся с вами. Если у вас есть вопросы,
                            вы можете задать их, позвонив нам по телефону или написав нам на почту.
                        </div>

                        <div class="sub_header">
                            <div>
                                Телефон для связи: <a href="tel:+78129173813" class="phone link"><b>+7 (812) 917-38-13</b></a>
                            </div>
                            <div>
                                Наша почта:  <a href="mailto:order@makeit-da.ru" class="email link"><b>order@makeit-da.ru</b></a>
                            </div>

                        </div>

                    </div>

                </div>
									<script>
											//для метрики
											$(document).trigger("form-send",[<?$arPrams["FORM_ID"]?>]);
									</script>

            <? elseif($arResult['SUCCESS'] == "N"):?>
                <div id="form_send">
                    <div class="icon">
                        <svg height="128" width="282" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 282 128">
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.13 0.34 C 84.13 0.34 281.94 0.34 281.94 0.34 C 281.94 0.34 281.94 127.71 281.94 127.71 C 281.94 127.71 84.24 127.71 84.24 127.71 C 84.24 127.71 84.13 0.34 84.13 0.34 Z" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.38 0.14 C 84.38 0.14 167.65 84.01 171.11 87.46 C 174.56 90.92 182.86 98.85 193.91 87.79 C 204.96 76.74 280.81 0.3 280.81 0.3 C 280.81 0.3 84.38 0.14 84.38 0.14 Z" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 280.75 127.24 C 280.75 127.24 217.91 64.4 217.91 64.4" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 84.74 126.56 C 84.74 126.56 147.4 63.9 147.4 63.9" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.96 33.85 C 73.96 33.85 0.53 33.85 0.53 33.85" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.92 64.77 C 73.92 64.77 45.25 64.77 45.25 64.77" />
                            <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 74.09 94.77 C 74.09 94.77 23.09 94.77 23.09 94.77" />
                        </svg>
                    </div>
                    
                    <!--<div><?/*var_dump($arResult["B24_RESULT"]);*/?></div>-->

                    <h2>Не удалось отправить форму</h2>

                    <div class="sub_header">
                    Скорее всего вы слишком часто ее заполняли или произошел технический сбой. <br>
                    Для того, чтобы связаться с нами, пожалуйста используйте контактную информацию, указанную ниже:
                    </div>

                    <a href="tel:+78122440149" class="phone">+7 (812) 244-01-49</a>
                    <a href="mailto:order@makeit-da.ru" class="email">order@makeit-da.ru</a>
                </div>
            <? endif?>
        </div>

        </div>
        </div>

    </div>

    <div class="overflow"></div>
</div>

<script>

	 <?
		//передаем данные от всех форм с файлом
		$files = array();
		foreach($arResult['FIELDS'] as $k=>$v){
			if($v['TYPE'] == 'file'){
				$files[] = $v;
			}
		}
	?>
	var files = <?=CUtil::PhpToJSObject($files)?>;
	BX.onCustomEvent('onComponentFormNewReady',[files]);

</script>

<!--/noindex-->