
function componentFormAlways(files){

	$('#order input').each(function(){
		var self = $(this);
		if(typeof self.data('mask') == "string"){
			self.mask(self.data('mask'));
		}
	});


	//console.log(files);

	for(var i = 0; i < files.length; i++){
		var self = $(this);
		var file = files[i];
		//не получается получить параметры



		//даем понятные для плагина формат для файлов
		file.FILES_SUPPORT = file.FILES_SUPPORT.replace(/ /g,'');
		if(file.FILES_SUPPORT.indexOf(',')){
			file.FILES_SUPPORT = file.FILES_SUPPORT.split(',');
			if(typeof file.FILES_SUPPORT == 'object' || typeof file.FILES_SUPPORT == 'array'){
				for(var ii = 0; ii < file.FILES_SUPPORT.length; ii++){
					file.FILES_SUPPORT[ii] = '.'+file.FILES_SUPPORT[ii];
				}
				file.FILES_SUPPORT = file.FILES_SUPPORT.join(', ');
			}
		}



		$("#"+file.NAME).dropzone({
                clickable:true,
				url: file.FILE_UPLOAD_PATH,
				paramName: file.NAME,
				maxFilesize:  file.MAX_FILE_SIZE,
				maxFiles: file.MAX_FILES,
				acceptedFiles: file.FILES_SUPPORT,

				init: function() {
					//console.log(file);
					//существующие файлы
					var thisDropzone = this;
					//размер слайдера
					/*thisDropzone.on("addedfile", function() {
						 reloadOrderlider();
					});
					thisDropzone.on("removedfile", function() {
						 reloadOrderlider();
					});*/



					if(typeof file.VALUE != 'undefined' && file.VALUE.length){
						var files = file.VALUE.join(',');
						$.get(file.FILE_UPLOAD_PATH+'?files='+files , function(data) {
							$.each(data, function(key,value){
								var mockFile = { name: value.ORIGINAL_NAME, size: value.FILE_SIZE, fileID:value.ID };
								thisDropzone.options.addedfile.call(thisDropzone, mockFile);
							});
							$(thisDropzone.element).find('.dz-preview').each(function(i,e){
								$(this).find('input').val(files[i]);
							});
						});
						//размер
						//reloadOrderlider();
					}




				},
				success:function(file, response){

					for(var i = 0; i < response.length; i++){
						$(file.previewElement).find('input').val(response[i].FILE_ID);
					}
					//размер

				},
				dictInvalidFileType: 'Недопустимый формат файла',
				dictCancelUploadConfirmation: 'Вы уверены, что хотите прервать загрузку файла?',
				dictMaxFilesExceeded: 'Можно добавить не больше '+file.MAX_FILES+' файлов',
				dictFileTooBig: 'Файл слишком большой - {{filesize}}Мб. Максимальный размер файла - {{maxFilesize}}Мб.',
				previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n <input type='hidden' name='"+file.NAME+"[]' value=''>\n  <div class=\"dz-details\">\n <div class=\"dz-filename\"><span data-dz-name></span></div> <div class=\"dz-size\" data-dz-size></div> <div class=\"dz-del\" data-dz-remove><img src=\"/local/templates/main/img/close-btn.svg\" alt=\"Удалить файл\"/></div>\n  </div>\n <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>",

			// previewTemplate: "<div class=\"dz-preview dz-file-preview\" id=\"preview-template\">\n <input type='hidden' name='"+file.NAME+"[]' value=''>\n <div class=\"dz-details\">\n <div class=\"dz-filename\"><span data-dz-name></span></div>\n <div class=\"dz-size\" data-dz-size>\n</div>\n <div class=\"dz-del\" data-dz-remove>\n<img src=\"<?=SITE_TEMPLATE_PATH?>/img/close-btn.svg\" alt=\"Удалить файл\"/>\n </div>\n </div>\n <div class=\"dz-progress\">\n<span class=\"dz-upload\" data-dz-uploadprogress></span>\n</div>\n </div>",
				previewsContainer: '.file-previews',
		});


	};



	//ошибки
	var captchaEr = $('.make-error');
	captchaEr.removeClass('make-error');
	captchaEr.addClass('error');
	$('.g-recaptcha').mouseenter(function(){
		setTimeout(function(){
		captchaEr.removeClass('error');
		},500);
	})
	$('.need-error').each(function(i,e){
		var self = $(this);
		self.removeClass('need-error');
		setTimeout(function(){
			self.addClass('error');
		},i*150);
	})




	//обновляем слайдер
	//createOrderSlider();
}









BX.addCustomEvent('onComponentFormNewReady', function(files){
	componentFormAlways(files);
});


BX.ready(function(e) {
    Dropzone.autoDiscover = false;
    var b = $("body");
    var html = $('html');




});