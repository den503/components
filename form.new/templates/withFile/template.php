<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<!--noindex-->


<? if($arResult['SUCCESS'] != "N" && $arResult['SUCCESS'] != "Y"):?>
    <div class="h1">
        Заявка на вышивку
    </div>
    <? if(strlen($arResult['ERRORS'])>0):?>
    <div class="form-error">
        <?=$arResult['ERRORS']?>
    </div>
<? endif?>



    <? if($_POST['FORMA_SEND'] == "Y"){/*var_dump($arResult['FIELDS']);*/}?>

    <form name="<?=$arParams['FORM_NAME']?>" action="<?=$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/formdata" data-form>

        <? foreach($arResult['FIELDS'] as $k=>$v):?>


            <? if($v['TYPE'] == 'text'):?>
                <div class="form-field">
                    <? if(count($v['ERRORS'])):?>
                        <ul class="errors">
                            <? foreach($v['ERRORS'] as $error):?>
                                <li><?=$error?></li>
                                <? break?>
                            <? endforeach?>
                        </ul>
                    <? endif?>
                    <input class="input" type="text" <?=$v['REQ']=='Y'? 'required': ''?> id="<?=$k?>" name="<?=$v['NAME']?>" <?=$v['MASK']?'data-mask="'.$v['MASK'].'"':''?> value="<?=$v['VALUE']?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>">
                    <label for="<?=$k?>"><?=$v['LABEL']?></label>
                </div>
            <? elseif($v['TYPE'] == 'tel'):?>
                <div class="form-field">
                    <? if(count($v['ERRORS'])):?>
                        <ul class="errors">
                            <? foreach($v['ERRORS'] as $error):?>
                                <li><?=$error?></li>
                                <? break?>
                            <? endforeach?>
                        </ul>
                    <? endif?>
                    <input class="input" type="tel" <?=$v['REQ']=='Y'? 'required': ''?> id="<?=$k?>" name="<?=$v['NAME']?>" <?=$v['MASK']?'data-mask="'.$v['MASK'].'"':''?> value="<?=$v['VALUE']?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>">
                    <label for="<?=$k?>"><?=$v['LABEL']?></label>
                </div>
            <? elseif($v['TYPE'] == 'email'):?>
                <div class="form-field">
                    <? if(count($v['ERRORS'])):?>
                        <ul class="errors">
                            <? foreach($v['ERRORS'] as $error):?>
                                <li><?=$error?></li>
                                <? break?>
                            <? endforeach?>
                        </ul>
                    <? endif?>
                    <input class="input" type="email" <?=$v['REQ']=='Y'? 'required': ''?> id="<?=$k?>" name="<?=$v['NAME']?>" <?=$v['MASK']?'data-mask="'.$v['MASK'].'"':''?> value="<?=$v['VALUE']?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>">
                    <label for="<?=$k?>"><?=$v['LABEL']?></label>
                </div>
            <? elseif($v['TYPE'] == 'textarea'):?>
                <div class="form-field">
                    <textarea class="input" <?=$v['REQ']=='Y'? 'required': ''?> name="<?=$v['NAME']?>" id="<?=$k?>" placeholder="<?=$v['PLACEHOLDER']?>" maxlength="<?=$v['MAX_LEN']?>"><?=$v['VALUE']?></textarea>
                    <label for="<?=$k?>"><?=$v['LABEL']?></label>

                    <? if(count($v['ERRORS'])):?>
                        <ul class="errors">
                            <? foreach($v['ERRORS'] as $error):?>
                                <li><?=$error?></li>
                                <? break?>
                            <? endforeach?>
                        </ul>
                    <? endif?>
                </div>
            <? elseif($v['TYPE'] == 'select'):?>

            <? elseif($v['TYPE'] == 'radio'):?>

            <? elseif($v['TYPE'] == 'checkbox'):?>

            <? elseif($v['TYPE'] == 'file'):?>
                <div id="<?=$v['NAME']?>" class="file-load">
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <div class="dz-message" data-dz-message>
                        <svg xmlns="http://www.w3.org/2000/svg" width="58" height="53" viewBox="0 0 58 53"><g><g><g opacity=".2"><path fill="#4c382b" d="M56.338 26.24c-.923 0-1.661.717-1.661 1.613v14.684c0 3.994-3.348 7.234-7.447 7.234H10.77c-4.11 0-7.447-3.252-7.447-7.234V27.614c0-.897-.738-1.614-1.661-1.614S0 26.717 0 27.614v14.923C0 48.313 4.837 53 10.77 53h36.46C53.175 53 58 48.3 58 42.537V27.853c0-.884-.739-1.614-1.662-1.614z"/></g><g opacity=".2"><path fill="#4c382b" d="M27.858 39.52c.314.312.737.48 1.148.48.41 0 .834-.156 1.148-.48L40.52 29.23c.64-.636.64-1.656 0-2.291a1.635 1.635 0 0 0-2.308 0l-7.575 7.532V1.619c0-.9-.725-1.619-1.631-1.619s-1.631.72-1.631 1.62v32.85l-7.587-7.531a1.635 1.635 0 0 0-2.308 0 1.606 1.606 0 0 0 0 2.29z"/></g></g></g></svg>
                        <span>Прикрепить файл(-ы) макета (до 5 файлов)</span>
                    </div>
                </div>
                <div class="file-previews"></div>
            <? elseif($v['TYPE'] == 'hidden'):?>
                <input type="hidden" name="<?=$v['NAME']?>" id="<?=$k?>" value="<?=$v['VALUE']?>">
            <? endif?>

        <? endforeach?>
        <div class="form-footer">
            <div class="note">Нажимая на кнопку вы соглашаетесь на <a href="<?=SITE_TEMPLATE_PATH?>/img/Politika_konfidentsialnosti_stezhkoff.pdf" target="_blank">обработку
                    персональных данных</a></div>
            <button class="btn" value="Y" name="FORMA_SEND" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
        </div>

        <div class="dz-preview dz-file-preview" id="preview-template">
            <input type='hidden' name='"+file.NAME+"[]' value=''>
            <div class="dz-details">
                <div class="dz-filename"><span data-dz-name></span></div>
                <div class="dz-size" data-dz-size></div>
                <div class="dz-del" data-dz-remove><img src="<?=SITE_TEMPLATE_PATH?>/img/close-btn.svg" alt="Удалить файл"/>
                </div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
        </div>
    </form>





<? elseif($arResult['SUCCESS'] == "Y"):?>

    <div class="success">
        <div class="icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="241" height="149" viewBox="0 0 241 149"><defs><clipPath id="84fua"><path fill="#fff" d="M2 62.241l83.52 83.632L243.576-2"/></clipPath></defs><g><g><path fill="none" stroke="#e35708" stroke-dasharray="7 7" stroke-miterlimit="50" stroke-width="8" d="M2 62.241v0l83.52 83.632v0L243.576-2v0" clip-path="url(&quot;#84fua&quot;)"/></g></g></svg>
        </div>
        <div class="h1"><?=GetMessage("SUCCESS")?></div>
        <div class="note"><?=GetMessage("SUCCESS_NOTE")?></div>
    </div>

<? elseif($arResult['SUCCESS'] == "N"):?>
    <div id="form_send">
        <div class="icon">
            <svg height="128" width="282" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 282 128">
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.13 0.34 C 84.13 0.34 281.94 0.34 281.94 0.34 C 281.94 0.34 281.94 127.71 281.94 127.71 C 281.94 127.71 84.24 127.71 84.24 127.71 C 84.24 127.71 84.13 0.34 84.13 0.34 Z" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="none" fill-rule="evenodd" d="M 84.38 0.14 C 84.38 0.14 167.65 84.01 171.11 87.46 C 174.56 90.92 182.86 98.85 193.91 87.79 C 204.96 76.74 280.81 0.3 280.81 0.3 C 280.81 0.3 84.38 0.14 84.38 0.14 Z" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 280.75 127.24 C 280.75 127.24 217.91 64.4 217.91 64.4" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 84.74 126.56 C 84.74 126.56 147.4 63.9 147.4 63.9" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.96 33.85 C 73.96 33.85 0.53 33.85 0.53 33.85" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 73.92 64.77 C 73.92 64.77 45.25 64.77 45.25 64.77" />
                <path stroke-linejoin="miter" stroke-linecap="butt" stroke-width="0.68" stroke="#000000" fill="#000000" fill-rule="evenodd" d="M 74.09 94.77 C 74.09 94.77 23.09 94.77 23.09 94.77" />
            </svg>
        </div>

        <!--<div><?/*var_dump($arResult["B24_RESULT"]);*/?></div>-->

        <h2>Не удалось отправить форму</h2>

        <div class="sub_header">
            Скорее всего вы слишком часто ее заполняли или произошел технический сбой. <br>
            Для того, чтобы связаться с нами, пожалуйста используйте контактную информацию, указанную ниже:
        </div>

        <a href="tel:+78122440149" class="phone">+7 (812) 244-01-49</a>
        <a href="mailto:order@makeit-da.ru" class="email">order@makeit-da.ru</a>
    </div>
<? endif?>

<script>

    <?
    //передаем данные от всех форм с файлом
    $files = array();
    foreach($arResult['FIELDS'] as $k=>$v){
        if($v['TYPE'] == 'file'){
            $files[] = $v;
        }
    }
    ?>
    var files = <?=CUtil::PhpToJSObject($files)?>;
    BX.onCustomEvent('onComponentFormNewReady',[files]);

</script>

<!--/noindex-->