<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * @global CUserTypeManager $USER_FIELD_MANAGER
 */

global $USER_FIELD_MANAGER;


$arComponentParameters = array(
	'PARAMETERS' => array(
        'CHECK' => array(
            'NAME' => GetMessage('M_SP_CHECK'),
            'TYPE' => 'LIST',
            'MULTIPLE' => "N",
            'VALUES' => array(
                "EMAIL" => GetMessage('M_SP_EMAIL'),
                "LOGIN" => GetMessage('M_SP_LOGIN')
            ),
            'ADDITIONAL_VALUES' => "Y"
        ),
        'EMAIL_ID' => array(
            'NAME' => GetMessage('M_SP_ID_SEND'),
            'TYPE' => 'STRING',
            'MULTIPLE' => "N",
        ),
	),
);