<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;
use \Bitrix\Catalog;
use \Bitrix\Catalog\ProductTable;
use Bitrix\Highloadblock as HL;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */
Loc::loadMessages(__FILE__);


class mSendPassword extends CBitrixComponent{
    protected $errors = [];

    function __construct($component = null){

        parent::__construct($component);

    }

    function onPrepareComponentParams($arParams){

        return $arParams;
    }

    //вывод
    function executeComponent(){
        global $APPLICATION;

        $this->arResult["FORM_ACTION"] = $_SERVER['REQUEST_URI'];
        $this->arResult["HIDDEN_FIELDS"] = array(
            "NAME" => "makeit_send_pwd_".$this->arParams["EMAIL_ID"],
            "REC_TRY" => "Y"
        );
        $this->arResult["CHECK_FIELD"] = $this->arParams["CHECK"];
        //var_dump($this->arParams);
        $stop = false;
        if($_POST["REC_TRY"] == "Y" && $_POST["NAME"] == $this->arResult["HIDDEN_FIELDS"]["NAME"]){
            $this->arResult["ERRORS"] = array();

            if(!$_POST[$this->arParams["CHECK"]]){
                $this->arResult["ERRORS"][] = "Не заполнено поле для определения пользователя";
                $stop = true;
            }else{
                $filter = array();
                $filter[$this->arParams["CHECK"]] = $_POST[$this->arParams["CHECK"]];

                $order = array('sort' => 'asc');
                $tmp = 'sort';
                $rsUsers = CUser::GetList($order, $tmp, $filter);
                $rsUsers->NavStart(1);
                $user = $rsUsers->Fetch();
                if(count($user) < 2) {$user = false;}

                if(!$user){
                    $this->arResult["ERRORS"][] = "Пользователь с указанными дагнными не найден";
                    $stop = true;
                }
            }

            if(!$stop){
                $newPass = $this->generate();


                $u = new CUser;
                $fields = Array(
                    "PASSWORD"          => $newPass,
                    "CONFIRM_PASSWORD"  => $newPass,
                );
                $res = $u->Update($user["ID"], $fields);

                if(!$res){
                    $this->arResult["ERRORS"][] = "Не удалось сменить пароль: ".$u->LAST_ERROR;
                }else{
                    $user["NAME"] = $user["NAME"]?$user["NAME"]:"Уважаемый покупатель";
                    $user["NEW_PASS"] = $newPass;
                    $eventID = CEvent::Send($this->arParams["EMAIL_ID"], SITE_ID, $user);



                    if(!$eventID){
                        $this->arResult["ERRORS"]["GLOBAL"][] = "Не удалось отправить письмо с паролем";
                    }else{
                        $this->arResult["SUCCESS"] = true;
                        $this->arResult["SUCCESS_EMAIL"] = $user["EMAIL"];
                    }


                }

            }




            var_dump($_POST);
            //сегерируем новй пароль
            //отправляем на почту




            $this->arResult["RESULT"] = true;
        }


        //$this->arResult["SORT_ITEMS"] = $f;
        //$this->arResult["SORT_SELECTED"] = $select;
        $this->arResult["ID"] = $this->arParams["ID"];


        $this->includeComponentTemplate();


    }

    function generate(){
        return randString(10, array(
            "abcdefghijklnmopqrstuvwxyz",
            "ABCDEFGHIJKLNMOPQRSTUVWX­YZ",
            "0123456789",
            "!@#\$%^&*()",
        ));
    }
}