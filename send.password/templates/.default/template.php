<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();} ?>

<? if($arResult["SUCCESS"]):?>
		<div class="head">
				<div class="h1">Пароль восстановлен</div>
				<div class="text">На почту <b><?=$arResult["SUCCESS_EMAIL"]?></b> отправлено письмо с новым паролем, который вы можете сменить в личном кабинете.</div>
		</div>
			<div class="success"></div>
<? else:?>
		<div class="head">
				<div class="h1">Восстановление пароля</div>
				<div class="text">Введите электронный адрес, указанный в личном кабинете. На него будет выслано письмо с паролем.</div>
		</div>
		<div class="pass-form">
				<form action="<?=$arResult["FORM_ACTION"]?>" method="POST" enctype="multipart/formdata">
						<? foreach($arResult["HIDDEN_FIELDS"] as $fCode => $fValue):?>
								<input type="hidden" name="<?=$fCode?>" value="<?=$fValue?>">
						<? endforeach?>

						<div class="form-field">
								<input type="text" name="<?=$arResult["CHECK_FIELD"]?>" id="rec_<?=$arResult["CHECK_FIELD"]?>">
								<label for="rec_<?=$arResult["CHECK_FIELD"]?>">Email</label>
						</div>
						<input class="custom-btn" type="submit" value="Отправить">
				</form>
		</div>
<? endif?>

