<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("M_SORTING_NAME"),
	"DESCRIPTION" => GetMessage("M_SORTING_DESC"),
	"CACHE_PATH" => "Y",
	"SORT" => 10,
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "sort",
			"NAME" => GetMessage("M_SORTING_CHILD"),
			"SORT" => 10,
		),
	),
);

?>