<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * @global CUserTypeManager $USER_FIELD_MANAGER
 */

global $USER_FIELD_MANAGER;


$arComponentParameters = array(
	'GROUPS' => array(
		'LANG' => array(
			'NAME' => GetMessage('M_GROUP_LANG'),
			'SORT' => 100
		),
	),
	'PARAMETERS' => array(
        'ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('M_SORT_ID'),
            'TYPE' => 'STRING',
            'MULTIPLE' => "N",
            'DEFAULT' => "SORT_1",
        ),
		'LANG_LIST' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('M_LANG_LIST'),
            'TYPE' => 'STRING',
            'MULTIPLE' => "Y",
            'REFRESH' => "Y"
		),
        'LANG_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('M_LANG_ID'),
            'TYPE' => 'STRING',
            'DEFAULT' => "ru",
        ),
        'SORT_FIELDS' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('M_SORT_FIELDS'),
            'TYPE' => 'STRING',
            'MULTIPLE' => "Y",
            'REFRESH' => "Y"
        ),







	),
);

foreach($arCurrentValues['SORT_FIELDS'] as $i=>$val){
    if(strlen($val)<=0) continue;
    $arComponentParameters["GROUPS"]["GROUP_".$val] = array(
        'NAME' => GetMessage('M_SORT_LANG_SECTION')." (".$val.')',
        'SORT' => $i*1000
    );

    $arComponentParameters['PARAMETERS']['GROUP_'.$i."_KEY"] = array(
        'PARENT' => "GROUP_".$val,
        'NAME' => GetMessage('M_SORT_KEY'),
        'TYPE' => 'STRING',
        "DEFAULT" => $val."_".$i
    );
    $arComponentParameters['PARAMETERS']['GROUP_'.$i."_VAL"] = array(
        'PARENT' => "GROUP_".$val,
        'NAME' => GetMessage('M_SORT_VAL'),
        'TYPE' => 'STRING',
    );

    //lang list
    foreach($arCurrentValues['LANG_LIST'] as $lang){
        if(strlen($lang)<=0) continue;
        $arComponentParameters['PARAMETERS']['GROUP_'.$i."_".$lang] = array(
            'PARENT' => "GROUP_".$val,
            'NAME' => GetMessage('M_SORT_LANG_PREFIX').' ('.$lang.')',
            'TYPE' => 'STRING',
        );
    }
}