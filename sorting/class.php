<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;
use \Bitrix\Catalog;
use \Bitrix\Catalog\ProductTable;
use Bitrix\Highloadblock as HL;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */
Loc::loadMessages(__FILE__);


class makeSort extends CBitrixComponent{
    protected $errors = [];

    function __construct($component = null){

        parent::__construct($component);

    }

    function onPrepareComponentParams($arParams){
        $arParams["LANG_LIST"] = array_diff(array_map('strtoupper', $arParams["LANG_LIST"]),array(""));
        if(array_search(strtoupper($arParams["LANG_ID"]),$arParams["LANG_LIST"]) === false){
            //error

        }else{
            $arParams["SELECTED_LANG_ID"] = strtoupper($arParams["LANG_ID"]);
        }

        return $arParams;
    }

    //вывод
    function executeComponent(){
        global $APPLICATION;

        //set sort PARAMETER
        if($_REQUEST["SORT"] && $_REQUEST["SORT"]["ID"] && $_REQUEST["SORT"]["FIELD"] && $_REQUEST["SORT"]["DIRECTION"]){
            $_SESSION["SORT"][$_REQUEST["SORT"]["ID"]] = array(
                "FIELD" => $_REQUEST["SORT"]["FIELD"],
                "DIRECTION" => $_REQUEST["SORT"]["DIRECTION"],
            );

            //$APPLICATION->GetCurPageParam("", array("SORT"),false);
            //var_dump($APPLICATION->GetCurPageParam("", array("SORT"),false));
            LocalRedirect($APPLICATION->GetCurPageParam("", array("SORT"),false),true);
            die();
        }


        $p = $this->arParams;
        $this->arResult = array();

        $acceptList = array(
            "KEY", "VAL", $p["SELECTED_LANG_ID"]
        );

        $f = array();
        foreach ($p as $key=>$val){
            if(strpos($key,"GROUP_")==false){continue;}

            $groupParams = explode("_",$key);
            $groupID = $groupParams[1];
            $paramCODE = strtoupper($groupParams[2]);

            if(!$paramCODE || !$val){continue;}//if null

            if(!$f[$groupID] && $val){
                $f[$groupID] = array(
                    "SORT" => intval($groupID)*10
                );
            }

            if(array_search($paramCODE,$acceptList) !== false){

                if(array_search($paramCODE,$p["LANG_LIST"]) !== false){
                    $paramCODE = "TEXT";
                }


                $f[$groupID][$paramCODE] = $val;
            }


        }

        //take selected
        $curSort = $_SESSION["SORT"][$this->arParams["ID"]];
        $select = false;
        foreach($f as &$group){
            $group["SELECTED"] = false;
            $group["PARAM"] = $group["KEY"]."#".$group["VAL"];
            if(
                ($group["KEY"] == $curSort["FIELD"] && $group["VAL"] == $curSort["DIRECTION"]) ||
                (!$curSort["FIELD"] || !$curSort["DIRECTION"])
            ){
                if(!$select){
                    $group["SELECTED"] = true;
                    $group["SORT"] = 9990000;
                    $select = $group;
                }
            }
            unset($group);
        }

        //default set
        if(!$curSort){
            $_SESSION["SORT"][$this->arParams["ID"]] = array(
                "FIELD" => $select["KEY"],
                "DIRECTION" => $select["VAL"],
            );
        }

        $this->arResult["UNSORT_ITEMS"] = $f;

        //sort fields, selected is last
        usort($f, function($a, $b) {
            return $a['SORT'] <=> $b['SORT'];
        });


        $this->arResult["SORT_ITEMS"] = $f;
        $this->arResult["SORT_SELECTED"] = $select;
        $this->arResult["ID"] = $this->arParams["ID"];


        $this->includeComponentTemplate();

        /*if($this->startResultCache(false)) {//1 hour
            $this->arResult = array();
            $this->arResult["URL"] = $this->GetPath()."/ajax.php";
            $this->arResult["LOAD_PARAMS"] = $this->arParams["LOAD_PARAMS"];
            $this->arResult["LOAD_PARAMS"]["LOAD"] = "Y";
            $this->arResult["LOAD_PARAMS"]["TEMPLATE"] = $this->GetTemplateName();

            $this->arResult["LOAD_PARAMS"] = json_encode($this->arResult["LOAD_PARAMS"],true);
            if($_REQUEST["LOAD"] == "Y"){
                $this->arResult["FIRST_RUN"] = false;
                try {
                    $inWidget = new \inWidget\Core($this->arParams["CONFIG"]);
                    $inWidget->getData();

                    $this->arResult["DATA"] = array(
                        "USER_NAME" => $inWidget->data->username,
                        "USER_AVATAR_SRC" => $inWidget->data->avatar,
                        "POSTS" => $inWidget->data->posts,
                        "FOLOWERS" => $inWidget->data->followers,
                        "FOLOWING" => $inWidget->data->following,
                        "BANNED" => $inWidget->isBannedUserId
                    );
                    $this->arResult["IMAGES"] = array();
                    foreach ($inWidget->data->images as $item){
                        $temp = array(
                            "AUTHOR_ID" => $item->authorId,
                            "LARGE" => $item->large,
                            "FULL" => $item->fullsize,
                            "SMALL" => $item->small,
                            "LINK" => $item->link,
                        );
                        $this->arResult["IMAGES"][] = $temp;
                    }
                    //$this->arResult
                } catch (\Exception $e) {
                    $this->arResult["ERROR"] = $e->getMessage();
                }
            }else{
                $this->arResult["FIRST_RUN"] = true;
            }


            $this->includeComponentTemplate();
        }*/
    }
}