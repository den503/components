<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();} ?>


<div class="catalog__navbar-sort sort-wrap" id="sort-wrap-<?=$arResult["ID"]?>">
    <div class="catalog__navbar-sort-title"><?=GetMessage("SORT_TITLE")?></div>
    <div class="dropdown">
        <div class="select">
            <span class="desktop"><?=$arResult["SORT_SELECTED"]["TEXT"]?></span>
            <span class="tablet"><?=GetMessage("SORT_TITLE_ALT")?></span>
            <div class="dropdown-arrow"></div>
        </div>
        <input type="hidden" class="sort-param" name="sort-param" value="<?=$arResult["SORT_SELECTED"]["PARAM"]?>" data-sort-id="<?=$arResult["ID"]?>">
        <ul class="dropdown-menu">
            <? foreach($arResult["SORT_ITEMS"] as $sort):?>
                <li data-val="<?=$sort["PARAM"]?>" class="sort-change"><?=$sort["TEXT"]?></li>
            <? endforeach?>
        </ul>
    </div>
</div>
<script>
    $("body").on("click","#sort-wrap-<?=$arResult['ID']?> .sort-change",function(){
        var self = $(this);
        var val = self.data("val");
        var parent = $(".sort-wrap");
        var input = parent.find(".sort-param");

        input.val(val).trigger("change");
    })

    $("body").on("change","#sort-wrap-<?=$arResult['ID']?> .sort-param",function(e){
        e.preventDefault();
        var self = $(this);
        var sortID = self.data("sort-id");
        var val = self.val().split("#");
        var queryStr = jQuery.param({SORT:{ID:sortID,FIELD:val[0],DIRECTION:val[1]}});
        var redirect = window.location.pathname+"?"+queryStr;
        window.location = redirect;
    })
</script>